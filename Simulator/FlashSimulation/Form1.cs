﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Pipes;

namespace FlashSimulator
{
    public partial class Form1 : Form
    {
        int size_flash = 0;
        byte[] data_source;
        int start_addr_show = 0;
        bool is_data_ready = false;
        Thread thread;
        private static NamedPipeServerStream server;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView_data_flash.ColumnCount = 32;
            dataGridView_data_flash.RowCount = 128;
            comboBox_displayType.SelectedIndex = 0;
            for (int i = 0; i < 32; i++)
            {
                DataGridViewColumn column = dataGridView_data_flash.Columns[i];
                dataGridView_data_flash.Columns[i].HeaderText = ""+i;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            for (int i = 0; i < 128; i++)
            {
                dataGridView_data_flash.Rows[i].HeaderCell.Value = (i * 32).ToString("X2");
                dataGridView_data_flash.RowHeadersWidth = 70;
            }
        }

        private void button_SaveFile_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog openFileDialog = new SaveFileDialog())
            {
                openFileDialog.Filter = "Binary(*.bin)|*.bin|Hex(*.hex)|*.hex|All File(*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.FileName != "")
                    {
                        Stream myStream = null;
                        if ((myStream = File.OpenWrite(openFileDialog.FileName)) != null)
                        {
                            myStream.Write(data_source, 0, (int)data_source.Length);
                        }
                        myStream.Close();
                    }
                }
            }
        }

        private string ByteArrayToString(byte[] ba, int ofset, int len)
        {
            byte[] data = new byte[len];
            Array.Copy(ba, ofset, data, 0, len);
            StringBuilder hex = new StringBuilder(data.Length * 2);
            foreach (byte b in data)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private void PipeTransfer()
        {
            server = new NamedPipeServerStream("TestPipe");
            server.WaitForConnection();
            StreamReader reader = new StreamReader(server);
            StreamWriter writer = new StreamWriter(server);
            while (true)
            {
                if (server.IsConnected == true)
                {
                    var line = reader.ReadLine();
                    if (line != null)
                    {
                        string[] cmd = line.Split(',');
                        if (cmd[0] == "read")
                        {
                            int addr = Convert.ToInt32(cmd[1]);
                            int len = Convert.ToInt32(cmd[2]);
                            string data_to_send = ByteArrayToString(data_source, addr, len);
                            writer.Write(data_to_send);
                            writer.Flush();
                        }
                        if(cmd[0] == "write")
                        {
                            int addr = Convert.ToInt32(cmd[1]);
                            int len = Convert.ToInt32(cmd[2]);
                            byte[] data_out = StringToByteArray(cmd[3]);
                            for (int i = 0; i < data_out.Length; i++)
                                data_out[i] = (byte)(data_out[i] & data_source[addr + i]);
                            Array.Copy(data_out, 0, data_source, addr, len);
                            textBox_AddrShow_TextChanged(null, null); //active show
                        }
                        if (cmd[0] == "erase")
                        {
                            int addr = Convert.ToInt32(cmd[1]);
                            byte[] data_out = new byte[4096];
                            int start_sector;
                            if ((addr % 4096) != 0)
                            {
                                start_sector = (addr/4096 + 1) * 4096;
                            }
                            else
                            {
                                start_sector = (addr / 4096) * 4096;
                            }
                            for (int i = 0; i < 4096; i++)
                                data_out[i] = 0xff;
                            Array.Copy(data_out, 0, data_source, start_sector, 4096);
                            textBox_AddrShow_TextChanged(null, null); //active show
                        }
                        server.Disconnect();
                    }
                    else
                    {
                        server.Disconnect();
                        server.WaitForConnection();
                    }
                }
                else
                {
                    server.WaitForConnection();
                }
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (buttonStart.Text == "START")
                {
                    size_flash = Convert.ToInt32(textBox_sizeFlash.Text);
                    data_source = new byte[size_flash];
                    for (int i = 0; i < data_source.Length; i++)
                    {
                        data_source[i] = 0xff;
                    }
                    dataGridView_data_flash.Enabled = true;
                    textBox_AddrShow.Enabled = true;
                    textBox_sizeFlash.Enabled = false;
                    textBox_filePath.Enabled = true;
                    button_SaveFile.Enabled = true;
                    button_Open.Enabled = true;
                    textBox_AddrShow_TextChanged(null, null);
                    thread = new Thread(new ThreadStart(PipeTransfer));
                    thread.IsBackground = true; // <-- Set your thread to background
                    // Start thread  
                    thread.Start();
                    buttonStart.Text = "STOP";
                }
                else
                {
                    buttonStart.Text = "START";
                    textBox_sizeFlash.Enabled = true;
                    textBox_AddrShow.Enabled = false;
                    dataGridView_data_flash.Enabled = false;
                    textBox_filePath.Enabled = false;
                    button_SaveFile.Enabled = false;
                    button_Open.Enabled = false;
                }
            }
            catch
            {
                MessageBox.Show("Set Size of Flash");
            }
        }

        private void textBox_AddrShow_TextChanged(object sender, EventArgs e)
        {
            try
            {
                is_data_ready = false;
                start_addr_show = Convert.ToInt32(textBox_AddrShow.Text);

                for (int i = 0; i < 4096; i++)
                {
                    if(start_addr_show + i >= data_source.Length)
                    {
                        dataGridView_data_flash.Rows[i / 32].Cells[i % 32].Value = "";
                        continue;
                    }
                    this.Invoke((MethodInvoker)delegate {
                        if (comboBox_displayType.SelectedItem.ToString() == "HEX")
                            dataGridView_data_flash.Rows[i / 32].Cells[i % 32].Value = data_source[start_addr_show + i].ToString("X2");
                        if (comboBox_displayType.SelectedItem.ToString() == "DEC")
                            dataGridView_data_flash.Rows[i / 32].Cells[i % 32].Value = data_source[start_addr_show + i].ToString("D");
                        if (comboBox_displayType.SelectedItem.ToString() == "ASCI")
                            dataGridView_data_flash.Rows[i / 32].Cells[i % 32].Value = Convert.ToChar(data_source[start_addr_show + i]);

                    });
                }
                is_data_ready = true;
            }
            catch
            {

            }
        }

        private void button_Open_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Binary(*.bin)|*.bin|Hex(*.hex)|*.hex|All File(*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    textBox_filePath.Text = openFileDialog.FileName;
                    Stream myStream = null;
                    if ((myStream = File.OpenRead(textBox_filePath.Text)) != null)
                    {
                        Array.Clear(data_source, 0, size_flash);
                        if (size_flash > myStream.Length)
                        {
                            myStream.Read(data_source, 0, (int)myStream.Length);
                        }
                        else
                        {
                            myStream.Read(data_source, 0, size_flash);
                        }
                        textBox_AddrShow_TextChanged(null, null); //active show
                        myStream.Close();
                    }
                }
            }
        }

        private void comboBox_displayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_AddrShow_TextChanged(null, null); //active show
        }

        private void dataGridView_data_flash_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (is_data_ready == true)
            {
                if (comboBox_displayType.SelectedItem.ToString() == "HEX")
                    data_source[e.RowIndex * 32 + e.ColumnIndex] = StringToByteArray(dataGridView_data_flash.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString())[0];
                if (comboBox_displayType.SelectedItem.ToString() == "DEC")
                    data_source[e.RowIndex * 32 + e.ColumnIndex] = (byte)Convert.ToInt32(dataGridView_data_flash.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                if (comboBox_displayType.SelectedItem.ToString() == "ASCI")
                    data_source[e.RowIndex * 32 + e.ColumnIndex] = Encoding.ASCII.GetBytes(dataGridView_data_flash.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString())[0];
            }
        }

        private byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
