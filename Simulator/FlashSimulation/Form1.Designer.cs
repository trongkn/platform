﻿namespace FlashSimulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView_data_flash = new System.Windows.Forms.DataGridView();
            this.comboBox_displayType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_AddrShow = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_sizeFlash = new System.Windows.Forms.TextBox();
            this.textBox_filePath = new System.Windows.Forms.TextBox();
            this.button_SaveFile = new System.Windows.Forms.Button();
            this.button_Open = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_data_flash)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_data_flash
            // 
            this.dataGridView_data_flash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_data_flash.Enabled = false;
            this.dataGridView_data_flash.Location = new System.Drawing.Point(12, 43);
            this.dataGridView_data_flash.Name = "dataGridView_data_flash";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_data_flash.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_data_flash.Size = new System.Drawing.Size(1074, 395);
            this.dataGridView_data_flash.TabIndex = 0;
            this.dataGridView_data_flash.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_data_flash_CellValueChanged);
            // 
            // comboBox_displayType
            // 
            this.comboBox_displayType.FormattingEnabled = true;
            this.comboBox_displayType.Items.AddRange(new object[] {
            "HEX",
            "DEC",
            "ASCI"});
            this.comboBox_displayType.Location = new System.Drawing.Point(389, 14);
            this.comboBox_displayType.Name = "comboBox_displayType";
            this.comboBox_displayType.Size = new System.Drawing.Size(68, 21);
            this.comboBox_displayType.TabIndex = 1;
            this.comboBox_displayType.SelectedIndexChanged += new System.EventHandler(this.comboBox_displayType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(345, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Display";
            // 
            // textBox_AddrShow
            // 
            this.textBox_AddrShow.Enabled = false;
            this.textBox_AddrShow.Location = new System.Drawing.Point(221, 14);
            this.textBox_AddrShow.Name = "textBox_AddrShow";
            this.textBox_AddrShow.Size = new System.Drawing.Size(79, 20);
            this.textBox_AddrShow.TabIndex = 3;
            this.textBox_AddrShow.Text = "0";
            this.textBox_AddrShow.TextChanged += new System.EventHandler(this.textBox_AddrShow_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(508, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "SizeFlash";
            // 
            // textBox_sizeFlash
            // 
            this.textBox_sizeFlash.Location = new System.Drawing.Point(560, 14);
            this.textBox_sizeFlash.Name = "textBox_sizeFlash";
            this.textBox_sizeFlash.Size = new System.Drawing.Size(65, 20);
            this.textBox_sizeFlash.TabIndex = 5;
            // 
            // textBox_filePath
            // 
            this.textBox_filePath.Enabled = false;
            this.textBox_filePath.Location = new System.Drawing.Point(788, 14);
            this.textBox_filePath.Name = "textBox_filePath";
            this.textBox_filePath.Size = new System.Drawing.Size(190, 20);
            this.textBox_filePath.TabIndex = 7;
            // 
            // button_SaveFile
            // 
            this.button_SaveFile.Enabled = false;
            this.button_SaveFile.Location = new System.Drawing.Point(1034, 14);
            this.button_SaveFile.Name = "button_SaveFile";
            this.button_SaveFile.Size = new System.Drawing.Size(50, 23);
            this.button_SaveFile.TabIndex = 8;
            this.button_SaveFile.Text = "Save";
            this.button_SaveFile.UseVisualStyleBackColor = true;
            this.button_SaveFile.Click += new System.EventHandler(this.button_SaveFile_Click);
            // 
            // button_Open
            // 
            this.button_Open.Enabled = false;
            this.button_Open.Location = new System.Drawing.Point(984, 14);
            this.button_Open.Name = "button_Open";
            this.button_Open.Size = new System.Drawing.Size(44, 23);
            this.button_Open.TabIndex = 9;
            this.button_Open.Text = "Open";
            this.button_Open.UseVisualStyleBackColor = true;
            this.button_Open.Click += new System.EventHandler(this.button_Open_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStart.Location = new System.Drawing.Point(12, 7);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(63, 30);
            this.buttonStart.TabIndex = 10;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 450);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.button_Open);
            this.Controls.Add(this.button_SaveFile);
            this.Controls.Add(this.textBox_filePath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_sizeFlash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_AddrShow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_displayType);
            this.Controls.Add(this.dataGridView_data_flash);
            this.Name = "Form1";
            this.Text = "FlashSimulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_data_flash)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_data_flash;
        private System.Windows.Forms.ComboBox comboBox_displayType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_AddrShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_sizeFlash;
        private System.Windows.Forms.TextBox textBox_filePath;
        private System.Windows.Forms.Button button_SaveFile;
        private System.Windows.Forms.Button button_Open;
        private System.Windows.Forms.Button buttonStart;
    }
}

