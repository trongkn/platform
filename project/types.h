#ifndef __TYPES_H
#define __TYPES_H

typedef enum bool { false = 0, true } bool;	// Undefined size
#define null			0


typedef signed int          int32_t;
typedef signed char         int8_t;
typedef signed short        int16_t;
typedef signed long      	int64_t;

typedef unsigned int        uint32_t;
typedef unsigned char       uint8_t;
typedef unsigned short  	uint16_t;
typedef unsigned long   	uint64_t;  // other name for 32-bit integer

typedef float				float32_t;
#endif // __TYPES_H

