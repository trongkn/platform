#ifndef __SERIAL_FLASH_H
#define __SERIAL_FLASH_H

#include "types.h"

typedef struct
{
    void (*open)(void);
    int32_t (*read)(uint32_t address, uint8_t *data, uint32_t len);
    bool (*write)(uint32_t address, uint8_t *data, uint32_t len);
    void (*erase_sector)(uint32_t address);
    void (*erase)(void);
}serial_flash_obj;

extern serial_flash_obj seial_flash;

#endif // __SERIAL_FLASH_H
