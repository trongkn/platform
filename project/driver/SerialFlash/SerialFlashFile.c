#include "SerialFlash.h"
#include "stdio.h"
#include "string.h"

#define FLASH_SIZE      0x400000
#define SECTOR_SIZE     512

#define FILE_NAME       "./flash_simulator.bin"
FILE *file_flash;

static void open(void);
static int32_t read(uint32_t address, uint8_t *data, uint32_t len);
static bool write(uint32_t address, uint8_t *data, uint32_t len);
static void erase_sector(uint32_t address);
static void erase(void);

serial_flash_obj seial_flash_file = {
    .open = open,
    .read = read,
    .write = write,
    .erase_sector = erase_sector,
    .erase = erase
};

static void open(void)
{
    file_flash = fopen(FILE_NAME,"r");
    if(file_flash == 0)
    {
        uint8_t data[SECTOR_SIZE];
        memset(data, 0xff, SECTOR_SIZE);
        file_flash = fopen(FILE_NAME,"wb+");
        if(file_flash != 0)
        {
            for(int i = 0; i < FLASH_SIZE/SECTOR_SIZE; i++)
            {
                fwrite(data, 1, SECTOR_SIZE, file_flash);
            }
        }
    }
    if(file_flash != 0) fclose(file_flash);
}

static int32_t read(uint32_t address, uint8_t *data, uint32_t len)
{
    if(address > FLASH_SIZE) return 0;
    if(address + len > FLASH_SIZE) return 0;
    file_flash = fopen(FILE_NAME,"rb");
    if(file_flash != 0)
    {
        fseek( file_flash, address, SEEK_SET);
        fread(data, 1, len, file_flash);
        fclose(file_flash);
        return len;
    }
    return 0;
}

static bool write(uint32_t address, uint8_t *data, uint32_t len)
{
    if(address > FLASH_SIZE) return false;
    if(address + len > FLASH_SIZE) return false;
    uint8_t data_read[len];
    file_flash = fopen(FILE_NAME,"r+");
    if(file_flash != 0)
    {
        fseek( file_flash, address, SEEK_SET);
        fread(data_read, 1, len, file_flash);
        for(int i = 0; i < len; i++)
            data_read[i] = data[i] & data_read[i];
        fseek( file_flash, address, SEEK_SET);
        fwrite(data_read, 1, len, file_flash);
        fclose(file_flash);
        return true;
    }
    return false;
}

static void erase_sector(uint32_t address)
{
    uint32_t sector;
    if(address > FLASH_SIZE) return;
    sector = SECTOR_SIZE*(address/SECTOR_SIZE);
    uint8_t data_read[SECTOR_SIZE];
    file_flash = fopen(FILE_NAME,"wb");
    if(file_flash != 0)
    {
        memset(data_read, 0xff, SECTOR_SIZE);
        fseek( file_flash, sector, SEEK_SET);
        fwrite(data_read, 1, SECTOR_SIZE, file_flash);
        fclose(file_flash);
    }
}

static void erase(void)
{
    uint8_t data_read[SECTOR_SIZE];
    file_flash = fopen(FILE_NAME,"wb");
    if(file_flash != 0)
    {

        memset(data_read, 0xff, SECTOR_SIZE);
        for(int i = 0; i < FLASH_SIZE/SECTOR_SIZE; i++)
        {
            fwrite(data_read, 1, SECTOR_SIZE, file_flash);
        }
        fclose(file_flash);
    }
}
