#include "SerialFlash.h"
#include "stdio.h"
#include "string.h"
#include <windows.h>
#include <tchar.h>
#include "common/log.h"
#include "types.h"

#define FLASH_SIZE      0x400000
#define SECTOR_SIZE     4096

#define PIPE_NAME _T("\\\\.\\pipe\\TestPipe")
#define BUFF_SIZE (SECTOR_SIZE * 2 + 100)

static void open(void);
static int32_t read(uint32_t address, uint8_t *data, uint32_t len);
static bool write(uint32_t address, uint8_t *data, uint32_t len);
static void erase_sector(uint32_t address);
static void erase(void);

serial_flash_obj seial_flash = {
    .open = open,
    .read = read,
    .write = write,
    .erase_sector = erase_sector,
    .erase = erase
};


TCHAR chBuff[BUFF_SIZE];
TCHAR chSend[19];
HANDLE hPipe;

#define nullptr 0

static char HexCode(unsigned char digit)
{
	char _hexcode[16] = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
	if (digit < 16)
	{
		return _hexcode[digit];
	}
	else return 0;
}

static void ConvertHex2S (TCHAR *buff_d, TCHAR *buff_s, uint16_t num_of_byte)
{
	uint16_t i = 0, j = 0;
	  TCHAR temp;
	  for (i = 0; i < num_of_byte; i++){
	  	temp =  HexCode((buff_s[i] >> 4) & 0x0f);
	    buff_d[j++] = temp;
	   	temp = HexCode((buff_s[i])      & 0x0f);
	    buff_d[j++] = temp;
	  }
	  buff_d[j++] = '\0';
}

static void ConvertString2Hex(TCHAR *buff_hex, TCHAR *byte, uint16_t numb)
{
  // src: byte
  // dis: buff_hex
  // numb: length of byte
	uint16_t i = 0;
	uint16_t j = 0;
	TCHAR temp_H = 0, temp_L = 0;
	TCHAR *ptr_c;
	TCHAR *ptr_h;
	ptr_h = buff_hex;
	ptr_c = byte;
	/* Convert data from char to hex */
	for (i = 0; i < numb; i = i + 2)
	{
		if (ptr_c[i] >= 'a' && ptr_c[i] <= 'f')
		{
		  temp_H = ptr_c[i] - 87;
		}
		else
		{
		  if (ptr_c[i] >= 'A' && ptr_c[i] <= 'F')
		  {
			temp_H = ptr_c[i] - 55;
		  }
		  else
		  {
			  temp_H = ptr_c[i] - 48;
		  }
		}
		if (ptr_c[i+1] >= 'a' && ptr_c[i+1] <= 'f')
		{
		  temp_L = ptr_c[i+1] - 87;
		}
		else
		{
		  if (ptr_c[i+1] >= 'A' && ptr_c[i+1] <= 'F')
		  {
			temp_L = ptr_c[i+1] - 55;
		  }
		  else
		  {
			  temp_L = ptr_c[i+1] - 48;
		  }
		}
		ptr_h[j] = ((temp_H << 4) & 0xf0) | (temp_L & 0x0f);
		j++;
	}
}

static void open(void)
{

}

static int32_t read(uint32_t address, uint8_t *data, uint32_t len)
{
	bool success;
	DWORD read_len;
	DWORD dwSend = 0;
	DWORD total_read = 0;
	TCHAR chSend[BUFF_SIZE];
	TCHAR chData[BUFF_SIZE-100];
    if(address > FLASH_SIZE) return 0;
    if(address + len > FLASH_SIZE) return 0;
    sprintf(chSend, "read,%d,%d\r\n", address, len);
	hPipe = CreateFile(PIPE_NAME, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);

	if (hPipe == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	DWORD mode = PIPE_READMODE_MESSAGE;
	SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);
    success = WriteFile(hPipe, chSend, strlen(chSend), &dwSend, 0);
    while(success == true)
	{
		success = ReadFile(hPipe, chData + total_read, BUFF_SIZE * sizeof(TCHAR), &read_len, nullptr);
		if(success == true)
		{
			total_read += read_len;
			chData[total_read] = 0;
			ConvertString2Hex((TCHAR*)data, chData, total_read);
		}
	}
    CloseHandle(hPipe);
    return total_read/2;
}

static bool write(uint32_t address, uint8_t *data, uint32_t len)
{
	bool success;
	DWORD dwSend = 0;
	TCHAR chSend[BUFF_SIZE];
	TCHAR chData[BUFF_SIZE-100];
    if(address > FLASH_SIZE) return false;
    if(address + len > FLASH_SIZE) return false;
    ConvertHex2S(chData, (TCHAR*)data, len);
    sprintf(chSend, "write,%d,%d,%s\r\n", address, len, chData);
	hPipe = CreateFile(PIPE_NAME, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);

	if (hPipe == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	DWORD mode = PIPE_READMODE_MESSAGE;
	SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);
    success = WriteFile(hPipe, chSend, strlen(chSend), &dwSend, 0);
    if(success == true)
    {
		CloseHandle(hPipe);
		return true;
    }
    return false;
}

static void erase_sector(uint32_t address)
{
	DWORD dwSend = 0;
	TCHAR chSend[BUFF_SIZE];
    sprintf(chSend, "erase,%d\r\n", address);
	hPipe = CreateFile(PIPE_NAME, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		return;
	}
	DWORD mode = PIPE_READMODE_MESSAGE;
	SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);
    WriteFile(hPipe, chSend, strlen(chSend), &dwSend, 0);
	CloseHandle(hPipe);
    return;
}

static void erase(void)
{
	DWORD dwSend = 0;
	TCHAR chSend[BUFF_SIZE];
    sprintf(chSend, "erase_all\r\n");
	hPipe = CreateFile(PIPE_NAME, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		return;
	}
	DWORD mode = PIPE_READMODE_MESSAGE;
	SetNamedPipeHandleState(hPipe, &mode, nullptr, nullptr);
    WriteFile(hPipe, chSend, strlen(chSend), &dwSend, 0);
	CloseHandle(hPipe);
    return;
}
