#ifndef __TIMER_H
#define __TIMER_H

#include "types.h"

typedef struct timer_obj
{
    uint32_t period;
    void (*timer_handler_ptr)(void);
    void (*start)(struct timer_obj* obj);
    void (*stop)(struct timer_obj* obj);
}timer_obj;

timer_obj* create_timer(uint32_t period, void (*timer_func_handler)(void));

#endif


