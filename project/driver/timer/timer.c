#include "timer.h"
#include "common.h"
#include <windows.h>

static void start_timer(timer_obj* obj);
static void stop_timer(timer_obj* obj);


static timer_obj timer1 = {
                    .period = 1000,
                    .start = start_timer,
                    .stop = stop_timer};

HANDLE win_timer;
VOID CALLBACK timer_sig_handler(PVOID, BOOLEAN);

timer_obj* create_timer(uint32_t period, void (*timer_func_handler)(void))
{
	timer_obj* timer;
	timer1.period = period;
	timer1.timer_handler_ptr = timer_func_handler;
	timer = &timer1;
	return timer;
}

static void start_timer(timer_obj* obj)
{
  CreateTimerQueueTimer(&win_timer, 0, (WAITORTIMERCALLBACK)timer_sig_handler, 0, timer1.period, timer1.period, WT_EXECUTEINTIMERTHREAD);
}


VOID CALLBACK timer_sig_handler(PVOID lpParameter, BOOLEAN TimerOrWaitFired)
{
  timer1.timer_handler_ptr();
}


static void stop_timer(timer_obj* obj)
{
  DeleteTimerQueueTimer(0, win_timer, 0);
  CloseHandle(win_timer);
}













