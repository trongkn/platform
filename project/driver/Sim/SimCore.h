#ifndef __SIM_CORE_H
#define __SIM_CORE_H

#include "types.h"

typedef struct
{
    bool (*turn_on)(void);
    bool (*config)(void);
    bool (*check_signal)(void);
    bool (*init_network)(void);
    //Sms sevice
    bool (*sms_send)(uint8_t *phone, uint8_t *data, uint32_t len);
    void (*sms_revc)(uint8_t *phone, uint8_t *data, uint32_t *len);
    // tcp_ip service
    bool (*tcp_connect)(uint8_t *ip, uint16_t port);
    bool (*tcp_send)(uint8_t *data, uint32_t len);
    bool is_connected;
    void (*tcp_close)();
    void (*tcp_revc)(uint8_t *data, uint32_t *len);
    // call service
    bool (*call_make)(uint8_t *phone);
    bool (*call_recv)(uint8_t *phone);
    bool (*call_end)(void);
    bool (*turn_off)(void);
    void (*handle)(void);
    //private variable
    uint16_t port;
    uint8_t ipv4[16];
}sim_obj;

extern sim_obj sim_module;

#endif // __SERIAL_FLASH_H
