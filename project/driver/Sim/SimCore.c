#include "SimCore.h"
#include <winsock2.h>
#include <windows.h>
#include "common/log.h"

WSADATA wsa;
SOCKET s;
struct sockaddr_in server;

char recvbuf[512];
int iResult;
int recvbuflen = 512;

static bool turn_off(void);
static bool sms_send(uint8_t *phone, uint8_t *data, uint32_t len);
static bool tcp_connect(uint8_t *ip, uint16_t port);
static bool tcp_send(uint8_t *data, uint32_t len);
static void tcp_close();
static bool call_make(uint8_t *phone);
static bool turn_on(void);
static void handle(void);

sim_obj sim_module = {
    .port = 0,
    .ipv4 = {0},
    .turn_off = turn_off,
    .sms_send = sms_send,
    .tcp_connect = tcp_connect,
    .tcp_send = tcp_send,
    .is_connected = false,
    .tcp_close = tcp_close,
    .call_make = call_make,
    .turn_on = turn_on,
    .handle = handle
};


static bool turn_off(void)
{
    WSACleanup();
    return true;
}

static bool sms_send(uint8_t *phone, uint8_t *data, uint32_t len)
{
    return true;
}

static bool tcp_connect(uint8_t *ip, uint16_t port)
{
    //Create a socket
    if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET)
    {
        return false;
    }
    server.sin_addr.s_addr = inet_addr(ip);
    server.sin_family = AF_INET;
    server.sin_port = htons( port );

    DWORD timeout = 1000;
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);

    if (connect(s , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        return false;
    }
    usleep(1000);
    sim_module.is_connected = true;
    return true;
}

static bool tcp_send(uint8_t *data, uint32_t len)
{
    if(send(s , data , len , 0) < 0)
    {
        return false;
    }
    return true;
}

static void tcp_close()
{
    shutdown(s, SD_SEND);
    closesocket(s);
    sim_module.is_connected = false;
}

static bool call_make(uint8_t *phone)
{
    return true;
}

static bool turn_on(void)
{
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        return false;
    }
    return true;
}

static void handle(void)
{
    if(sim_module.is_connected == true)
    {
        iResult = recv(s, recvbuf, recvbuflen, 0);
        if ( iResult > 0 )
        {
        	log_trace("Bytes received: %d\n");
        }
        else if ( iResult == 0 )
        {
            sim_module.is_connected = false;
            log_trace("Connection closed\n");
        }
    }
}
