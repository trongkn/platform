#include "serial.h"

//porting hardware
#include <windows.h>
HANDLE serial_port;
HANDLE open_serial_port(const char * device, uint32_t baud_rate);
SSIZE_T read_port(HANDLE port, uint8_t * buffer, size_t size);
int write_port(HANDLE port, uint8_t * buffer, size_t size);
//
static void SendByte(uint8_t byte);
static bool ReceiveByte(uint8_t *byte);
static void SerialSend(serial_obj* serial, uint8_t *data, uint16_t len);
static uint16_t SerialRead(serial_obj* serial, uint8_t *data, uint16_t len);
static void SerialHandle(serial_obj* serial);

serial_obj* create_serial(uint16_t port, uint16_t tx_size, uint16_t rx_size)
{
	serial_obj* serial;
	serial = MALLOC(sizeof(serial_obj));
	MEMSET(serial, 0, sizeof(serial_obj));
	if(serial != NULL)
	{
		serial->rx_buf = create_fifo(rx_size, 1);
		if(serial->rx_buf != NULL)
		{
			serial->tx_buf = create_fifo(tx_size, 1);
			if(serial->tx_buf != NULL)
			{
				serial->port = port;
				serial->send = SerialSend;
				serial->read = SerialRead;
				serial->handle = SerialHandle;
				//porting hardware
				  serial->SendByte = SendByte;
				  serial->ReceiveByte = ReceiveByte;
				  const char * device = "\\\\.\\COM2";
				  uint32_t baud_rate = 9600;
				  serial_port = open_serial_port(device, baud_rate);
				//
				return serial;
			}
			else
			{
				FREE(serial->rx_buf);
				FREE(serial);
				return NULL;
			}
		}
		else
		{
			FREE(serial);
			return NULL;
		}
	}
	return NULL;
}

static void SerialSend(serial_obj* serial, uint8_t *data, uint16_t len)
{
	serial->tx_buf->Puts(serial->tx_buf, data, len);
}

static uint16_t SerialRead(serial_obj* serial, uint8_t *data, uint16_t len)
{
	if(serial->rx_buf->Gets(serial->rx_buf, data, len) == true)
	{
		serial->count_received = serial->rx_buf->data_len;
		return len;
	}
	return 0;
}

static void SerialHandle(serial_obj* serial)
{
	uint8_t data;
	if(serial->tx_buf->data_len > 0)
	{
		serial->tx_buf->Get(serial->tx_buf, &data);
		serial->SendByte(data);

	}
	if(serial->ReceiveByte(&data) == true)
	{
		serial->rx_buf->Put(serial->rx_buf, &data);
	}
	serial->count_received = serial->rx_buf->data_len;
}

// Porting hardware
static void SendByte(uint8_t byte)
{
	write_port(serial_port, &byte, 1);
}

static bool ReceiveByte(uint8_t *byte)
{
	SSIZE_T size;
	size = read_port(serial_port, byte, 1);
	if(size > 0)
		return true;
	return false;
}

HANDLE open_serial_port(const char * device, uint32_t baud_rate)
{
  HANDLE port = CreateFileA(device, GENERIC_READ | GENERIC_WRITE, 0, NULL,
    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (port == INVALID_HANDLE_VALUE)
  {
    return INVALID_HANDLE_VALUE;
  }

  // Flush away any bytes previously read or written.
  BOOL success = FlushFileBuffers(port);
  if (!success)
  {
    CloseHandle(port);
    return INVALID_HANDLE_VALUE;
  }

  // Configure read and write operations to time out after 100 ms.
  COMMTIMEOUTS timeouts = {0};
  timeouts.ReadIntervalTimeout = 0;
  timeouts.ReadTotalTimeoutConstant = 1;
  timeouts.ReadTotalTimeoutMultiplier = 0;
  timeouts.WriteTotalTimeoutConstant = 0;
  timeouts.WriteTotalTimeoutMultiplier = 0;

  success = SetCommTimeouts(port, &timeouts);
  if (!success)
  {
    CloseHandle(port);
    return INVALID_HANDLE_VALUE;
  }

  // Set the baud rate and other options.
  DCB state = {0};
  state.DCBlength = sizeof(DCB);
  state.BaudRate = baud_rate;
  state.ByteSize = 8;
  state.Parity = NOPARITY;
  state.StopBits = ONESTOPBIT;
  success = SetCommState(port, &state);
  if (!success)
  {
    CloseHandle(port);
    return INVALID_HANDLE_VALUE;
  }

  return port;
}

// Writes bytes to the serial port, returning 0 on success and -1 on failure.
int write_port(HANDLE port, uint8_t * buffer, size_t size)
{
  DWORD written;
  BOOL success = WriteFile(port, buffer, size, &written, NULL);
  if (!success)
  {
    return -1;
  }
  if (written != size)
  {
    return -1;
  }
  return 0;
}

// Reads bytes from the serial port.
// Returns after all the desired bytes have been read, or if there is a
// timeout or other error.
// Returns the number of bytes successfully read into the buffer, or -1 if
// there was an error reading.
SSIZE_T read_port(HANDLE port, uint8_t * buffer, size_t size)
{
  DWORD received;
  BOOL success = ReadFile(port, buffer, size, &received, NULL);
  if (!success)
  {
    return -1;
  }
  return received;
}

// porting hardware
