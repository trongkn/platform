#ifndef __SERIAL_H
#define __SERIAL_H

#include "types.h"
#include "fifo.h"
#include "common.h"


#define MAX_BUF_RX			1024
#define MAX_BUF_TX			512

typedef struct serial_obj
{
	// for porting hardware
	uint16_t port;
	void (*SendByte)(uint8_t byte);
	bool (*ReceiveByte)(uint8_t *byte);
	// for platform
    fifo* tx_buf;
    fifo* rx_buf;
    uint16_t count_received;
    void (*send)(struct serial_obj* serial, uint8_t *data, uint16_t len);
    uint16_t (*read)(struct serial_obj* serial, uint8_t *data, uint16_t len);
    void (*handle)(struct serial_obj* serial);
}serial_obj;

serial_obj* create_serial(uint16_t port, uint16_t tx_size, uint16_t rx_size);

#endif


