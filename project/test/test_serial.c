/*
 * serial.c
 *
 *  Created on: Aug 4, 2021
 *      Author: VDT-MS
 */

#include "Serial/serial.h"
#include "types.h"
#include "timer/timer.h"

static serial_obj *serial;
static timer_obj *timer1;

static void handle(void)
{
	serial->handle(serial);
}

int test_serial(void)
{
	uint8_t data_buff[128];
	uint16_t len_recv = 0;
	serial = create_serial(0,1024, 1024);
	timer1  = create_timer(1, &handle);
	timer1->start(timer1);
	serial->send(serial, "nguyen cong phong", 17);
	while(1)
	{
		if(serial->count_received > 0)
		{
			len_recv = serial->read(serial, data_buff, serial->count_received);
			serial->send(serial, data_buff, len_recv);
		}
	}
	return 1;
}
