/*
 * test_fifo.c
 *
 *  Created on: Aug 4, 2021
 *      Author: VDT-MS
 */
#include "types.h"
#include "fifo.h"

fifo* ff_test;
fifo* ff2_test;

void test_fifo(void)
{
	uint8_t test_buf[4] = "0";
	ff_test = create_fifo(100, 4);
	ff2_test = create_fifo(100, 4);
	if(ff_test != null && ff2_test != null)
	{
		for(int i = 0; i < 200; i++)
		{
			if(ff_test->Put(ff_test, test_buf) == true)
			{
				sprintf(test_buf, "%d", i);
			}
			else
				break;
			sprintf(test_buf, "a%d", i);
			if(ff2_test->Put(ff2_test, test_buf) == true)
			{
				sprintf(test_buf, "%d", i);
			}
			else
				break;
		}
	}
	for(int i = 0; i < 200; i++)
	{
		if(ff_test->Get(ff_test, test_buf) == true)
		{
			;
		}
		else
			break;
	}
	for(int i = 0; i < 200; i++)
	{
		if(ff2_test->Get(ff2_test, test_buf) == true)
		{
			;
		}
		else
			break;
	}
}

