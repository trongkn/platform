/*
 * test_module_sim.c
 *
 *  Created on: Aug 4, 2021
 *      Author: VDT-MS
 */

#include "types.h"
#include "Sim/SimCore.h"
#include "timer/timer.h"

static timer_obj *timer1;

static void handle(void)
{
	sim_module.handle();
}

int test_module_sim()
{
	sim_module.turn_on();
	sim_module.tcp_connect("127.0.0.1", 3000);
	sim_module.tcp_send("nguyen cong phong", 17);
	timer1  = create_timer(1, &handle);
	timer1->start(timer1);
	while(1)
	{
		sim_module.tcp_send("nguyen cong phong", 17);
		sleep(1);
	}
}

