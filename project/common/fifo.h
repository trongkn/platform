/******************************************************************************
*
* M1 Communication Inc.
* (c) Copyright 2014 M1 Communication Inc.
* ALL RIGHTS RESERVED.
*
***************************************************************************//*!
*
* @file        fifo.h
*
* @author   phongnc 
* 
* @version  1.0
* 
* @date      
* 
* @brief     first in first out data type
* 		   
*
*******************************************************************************
*
* Detailed Description 
*
******************************************************************************/

#ifndef __FIFO_H_
#define __FIFO_H_

#include <string.h>
#include "types.h"

typedef struct fifo
{
    uint16_t read_idx;
    uint16_t write_idx;
    uint8_t *buffer;
    uint16_t data_len;
    uint16_t size;
    uint16_t size_element;
    bool (*Put)(struct fifo* fifo, void* byte);
    bool (*Puts)(struct fifo* fifo, void* buffer, uint16_t length);
    bool (*Peak)(struct fifo* fifo, void *data);
    bool (*Peaks)(struct fifo* fifo, void *data, uint16_t len);
    uint16_t (*Get)(struct fifo* fifo, void* byte);
    bool (*Gets)(struct fifo* fifo, void* buffer, uint16_t length);
    bool (*Clear)(struct fifo* fifo);
    uint16_t (*GetAvaiableSpace)(struct fifo* fifo);
}fifo;

fifo* create_fifo(uint16_t size, uint16_t element_size);


#endif /* FIFO_H_ */
