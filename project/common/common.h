#ifndef __COMMON_H
#define __COMMON_H

#include <stdlib.h>


#define 	MALLOC			malloc
#define 	FREE			free
#define		MEMSET			memset
#define 	MEMCPY			memcpy

#endif
