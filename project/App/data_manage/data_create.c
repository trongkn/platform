/*
 * data_flash.c
 *
 *  Created on: Aug 4, 2021
 *      Author: VDT-MS
 */

#include <data_manage/data_create.h>
#include <stdio.h>
#include <string.h>
#include "variable_manage/variable_manage.h"
#include <data_manage/data_flash.h>

#define	MAX_DATA_LEN 128

typedef union
{
	float Val;					/*!<   */
	unsigned char  v[4];		/*!<   */
}posistion_float_t;

DataContent* DATA_CreateJourneyData()
{
	posistion_float_t lat, lng;
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_JOURNEY;
		data->time = g_system_time;
		data->data = MALLOC(13);
		if(data->data != null)
		{
			data->len = 13;
			lat.Val = g_vehicle_data.Posistion.Lat;
			lng.Val = g_vehicle_data.Posistion.Lng;
			data->data[0] = lat.v[0];data->data[1] = lat.v[1];data->data[2] = lat.v[2];data->data[3] = lat.v[3];
			data->data[4] = lng.v[0];data->data[5] = lng.v[1];data->data[6] = lng.v[2];data->data[7] = lng.v[3];
			data->data[8] = g_vehicle_data.Speed.speed_gps;
			data->data[9] = g_vehicle_data.Speed.speed_machine;
			data->data[10] = g_vehicle_data.Posistion.OrGPS;data->data[11] = g_vehicle_data.Posistion.OrGPS>>8;
			data->data[12] = g_vehicle_data.MotoState.Val;
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

DataContent* DATA_CreateChangeDriverData(uint8_t *gplx_old, uint8_t *driver_name_old, uint8_t *gplx_new, uint8_t *driver_name_new)
{
	posistion_float_t lat, lng;
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_JOURNEY;
		data->time = g_system_time;
		data->data = MALLOC(128);
		if(data->data != null)
		{
			lat.Val = g_vehicle_data.Posistion.Lat;
			lng.Val = g_vehicle_data.Posistion.Lng;
			data->data[0] = lat.v[0];data->data[1] = lat.v[1];data->data[2] = lat.v[2];data->data[3] = lat.v[3];
			data->data[4] = lng.v[0];data->data[5] = lng.v[1];data->data[6] = lng.v[2];data->data[7] = lng.v[3];
			snprintf(&data->data[8], 120, "%s,%s,%s,%s", driver_name_old,
														gplx_old,
														driver_name_new,
														gplx_new);

			data->len = 8 + strlen(&data->data[8]);
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

DataContent* DATA_CreateDriverData(uint8_t *gplx, uint8_t *driver_name)
{
	posistion_float_t lat, lng;
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_JOURNEY;
		data->time = g_system_time;
		data->data = MALLOC(128);
		if(data->data != null)
		{
			lat.Val = g_vehicle_data.Posistion.Lat;
			lng.Val = g_vehicle_data.Posistion.Lng;
			data->data[0] = lat.v[0];data->data[1] = lat.v[1];data->data[2] = lat.v[2];data->data[3] = lat.v[3];
			data->data[4] = lng.v[0];data->data[5] = lng.v[1];data->data[6] = lng.v[2];data->data[7] = lng.v[3];
			snprintf(&data->data[8], 120, "%s,%s", g_driver_data.driver_name, g_driver_data.driver_license);
			data->len = 8 + strlen(&data->data[8]);
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

DataContent* DATA_CreateSpeed30sData(uint8_t *speed)
{
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_SPEED_30S;
		data->time = g_system_time;
		data->data = MALLOC(30);
		if(data->data != null)
		{
			MEMCPY(data->data, speed, 30);
			data->len = 30;
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

DataContent* DATA_CreateRunstopData()
{
	posistion_float_t lat, lng;
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_RUNSTOP;
		data->time = g_system_time;
		data->data = MALLOC(128);
		if(data->data != null)
		{
			lat.Val = g_vehicle_data.Posistion.Lat;
			lng.Val = g_vehicle_data.Posistion.Lng;
			data->data[0] = lat.v[0];data->data[1] = lat.v[1];data->data[2] = lat.v[2];data->data[3] = lat.v[3];
			data->data[4] = lng.v[0];data->data[5] = lng.v[1];data->data[6] = lng.v[2];data->data[7] = lng.v[3];
			data->data[8] = (uint8_t)g_driver_data.StopTimes; data->data[9] = (uint8_t)(g_driver_data.StopTimes >> 8);
			data->len = 10;
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

DataContent* DATA_CreateOverSpeedEvtData()
{

	return null;
}

DataContent* DATA_CreateRunstopEvtData()
{

	return null;
}

DataContent* DATA_CreateDriveReportData()
{

	return null;
}

DataContent* DATA_create_backup_infor_data()
{
	DataContent* data = null;
	data = MALLOC(sizeof(DataContent));
	if(data != null)
	{
		data->type = DATA_BACKUP_INFO;
		data->time = g_system_time;
		data->data = MALLOC(128);
		if(data->data != null)
		{
			data->data[0] = (uint8_t)g_flashdata_poiter.miss_poiter_data;
			data->data[1] = (uint8_t)(g_flashdata_poiter.miss_poiter_data >> 8);
			data->data[2] = (uint8_t)(g_flashdata_poiter.miss_poiter_data >> 16);
			data->data[3] = (uint8_t)(g_flashdata_poiter.miss_poiter_data >> 24);
			data->data[4] = (uint8_t)g_driver_data.Curr_RunTime; data->data[5] = (uint8_t)(g_driver_data.Curr_RunTime >> 8);
			data->data[6] = (uint8_t)g_driver_data.RunTimePDate; data->data[7] = (uint8_t)(g_driver_data.RunTimePDate >> 8);
			data->data[8] = (uint8_t)g_driver_data.StopTimes; data->data[9] = (uint8_t)(g_driver_data.StopTimes >> 8);
			snprintf(&data->data[10], 120, "%s,%s", g_driver_data.driver_name, g_driver_data.driver_license);
			data->len = 10 + strlen(&data->data[10]);
			return data;
		}
		else
			FREE(data);
	}
	return null;
}

void DATA_Delete(DataContent** data)
{
	DataContent* dt;
	dt = *data;
	if(dt != null)
	{
		FREE(dt->data);
		FREE(dt);
	}
	*data = null;
}
