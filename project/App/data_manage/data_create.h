/*
 * data_flash.h
 *
 *  Created on: Aug 4, 2021
 *      Author: VDT-MS
 */

#ifndef APP_DATA_MANAGE_DATA_CREATE_H_
#define APP_DATA_MANAGE_DATA_CREATE_H_

#include "types.h"
#include "common.h"

typedef enum 
{
	DATA_JOURNEY = 0,
	DATA_CHANGE_DRIVER,
	DATA_DRIVER,
	DATA_SPEED_30S,
	DATA_RUNSTOP,
	DATA_OVERSPEED_EVT,
	DATA_RUNSTOP_EVT,
	DATA_DRIVER_REPORT,
	DATA_BACKUP_INFO,
}DATA_TYPE;

typedef struct DataContent
{
	uint32_t time;
	uint8_t *data;
	DATA_TYPE type;
	uint16_t len;
}DataContent;


DataContent* DATA_CreateJourneyData();
DataContent* DATA_CreateChangeDriverData(uint8_t *gplx_old, uint8_t *driver_name_old, uint8_t *gplx_new, uint8_t *driver_name_new);
DataContent* DATA_CreateDriverData();
DataContent* DATA_CreateSpeed30sData(uint8_t *speed);
DataContent* DATA_CreateRunstopData();
DataContent* DATA_CreateOverSpeedEvtData();
DataContent* DATA_CreateRunstopEvtData();
DataContent* DATA_CreateDriveReportData();
DataContent* DATA_create_backup_infor_data();
void DATA_Delete(DataContent** data);
void DATA_SetMiss(DataContent* data);


#endif /* APP_DATA_MANAGE_DATA_CREATE_H_ */
