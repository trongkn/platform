/*
 * flash_save.h
 *
 *  Created on: Aug 14, 2021
 *      Author: VDT-MS
 */

#ifndef APP_DATA_MANAGE_DATA_FLASH_H_
#define APP_DATA_MANAGE_DATA_FLASH_H_


#include "data_create.h"

typedef struct
{
	uint32_t miss_poiter_data;
	uint32_t cur_poiter_data;
}PoiterFlashData;

extern PoiterFlashData g_flashdata_poiter;

bool DATAFLASH_BackupData(void);
DataContent* DATAFLASH_RestoreData(void);
uint32_t DATAFLASH_Init(DataContent *data);
bool DATAFLASH_Save(DataContent *data);
DataContent* DATAFLASH_Read(uint32_t addr, uint16_t *offset);
DataContent* DATAFLASH_ReadMiss(uint16_t *offset);

#endif /* APP_DATA_MANAGE_DATA_FLASH_H_ */
