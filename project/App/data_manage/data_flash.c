/*
 * flash_save.c
 *
 *  Created on: Aug 14, 2021
 *      Author: VDT-MS
 */

#include "data_flash.h"
#include "SerialFlash/SerialFlash.h"
#include "common.h"

#define ADDR_START_MEMORY			0
#define SECTOR_SIZE					4096
#define NUM_SECTOR_DATA				40
#define MAX_DATA_LEN				128

#define HEADER_1						'@'
#define HEADER_2						'@'
#define FOOSTER_1						'#'
#define FOOSTER_2						'#'

#define HEADER_POS1						0
#define HEADER_POS2						1
#define TIME_POS						2
#define TYPE_POS						6
#define DATA_LEN						7
#define DATA_POS						8
#define FOOSTER_POS1(x)					(8+x)
#define FOOSTER_POS2(x)					(9+x)




PoiterFlashData g_flashdata_poiter = {ADDR_START_MEMORY, ADDR_START_MEMORY};

static DataContent* DATAFLASH_ExtractData(uint8_t* data, uint16_t *out_pos);
static uint32_t DATAFLASH_CreateData(uint8_t* data_save, DataContent* data);

static DataContent* DATAFLASH_ExtractData(uint8_t* data, uint16_t *out_pos)
{
	uint32_t i;
	uint8_t len_byte;
	DataContent* ret;
	for(i = 0; i < MAX_DATA_LEN; i++)
	{
		if(data[i] == HEADER_1 && data[i+1] == HEADER_2)
		{
			len_byte = data[i+DATA_LEN];
			if(data[i+FOOSTER_POS1(len_byte)] == FOOSTER_1 && data[i+FOOSTER_POS2(len_byte)] == FOOSTER_2)
			{
				ret = MALLOC(sizeof(DataContent));
				if(ret != null)
				{
					ret->type = data[TYPE_POS];
					ret->time = (uint32_t)data[i+TIME_POS] + ((uint32_t)data[i+TIME_POS+1])*256 + ((uint32_t)data[i+TIME_POS+2])*65536 + ((uint32_t)data[i+TIME_POS+3])*16777216;
					ret->data = MALLOC(len_byte);
					ret->len = len_byte;
					if(ret->data != null)
					{
						MEMCPY(ret->data, &data[i+DATA_POS], len_byte);
						*out_pos = 1+i+FOOSTER_POS2(len_byte);
						return ret;
					}
					else
						FREE(ret);
				}
			}
		}
	}
	return null;
}

static uint32_t DATAFLASH_CreateData(uint8_t* data_save, DataContent* data)
{
	data_save[HEADER_POS1] = HEADER_1;
	data_save[HEADER_POS2] = HEADER_2;
	data_save[DATA_LEN] = data->len;
	data_save[TIME_POS] = (uint8_t)data->time;data_save[TIME_POS+1] = (uint8_t)(data->time >> 8);
	data_save[TIME_POS+2] = (uint8_t)(data->time >> 16);data_save[TIME_POS+3] = (uint8_t)(data->time >> 24);
	data_save[TYPE_POS] = data->type;
	MEMCPY(&data_save[DATA_POS], data->data, data->len);
	data_save[FOOSTER_POS1(data->len)] = FOOSTER_1;
	data_save[FOOSTER_POS2(data->len)] = FOOSTER_2;
	return FOOSTER_POS2(data->len) + 1;
}

bool DATAFLASH_BackupData(void)
{
	return false;
}

DataContent* DATAFLASH_RestoreData(void)
{
	uint32_t i, sector_select = 0, time_max = 0, last_addr;
	uint16_t pos;
	uint8_t data_read[SECTOR_SIZE];
	DataContent* ret = null;
	// Find last sector
	for(i = 0; i < NUM_SECTOR_DATA; i++)
	{
		pos = 0;
		seial_flash.read(ADDR_START_MEMORY + i * SECTOR_SIZE, data_read, MAX_DATA_LEN);
		ret = DATAFLASH_ExtractData(data_read, &pos);
		if(ret != null)
		{
			if(ret->time > time_max)
			{
				time_max = ret->time;
				sector_select = i;
			}
			DATA_Delete(&ret);
		}
		else  // ideal no data in next sector
		{
			break;
		}
	}
	// Find last packet
	seial_flash.read(ADDR_START_MEMORY + sector_select*4096, data_read, SECTOR_SIZE);
	last_addr = ADDR_START_MEMORY + sector_select*4096;
	i = 0;
	time_max = 0;
	sector_select = 0;
	while(1)
	{
		ret = DATAFLASH_ExtractData(data_read + i, &pos);
		if(ret != null)
		{
			if(ret->time >= time_max)
			{
				time_max = ret->time;
				sector_select = i;
			}
			DATA_Delete(&ret);
		}
		else
		{
			break;
		}
		i += pos;
	}
	//Read last packet
	ret = DATAFLASH_ExtractData(data_read + sector_select, &pos);
	if(ret != null)
	{
		g_flashdata_poiter.cur_poiter_data = last_addr + sector_select;
		if(ret->type == DATA_BACKUP_INFO)
		{
			g_flashdata_poiter.miss_poiter_data = (uint32_t)ret->data[0] + ((uint32_t)ret->data[1])*256 + ((uint32_t)ret->data[2])*65536 + ((uint32_t)ret->data[3])*16777216;
			last_addr += pos;
		}
		DATA_Delete(&ret);
	}
	return ret;
}

uint32_t DATAFLASH_Init(DataContent *data)
{
	return false;
}

bool DATAFLASH_Save(DataContent *data)
{
	uint8_t data_save[MAX_DATA_LEN];
	uint16_t len_out, try_write_times;
	bool is_new_sector = false, ret = false;
	uint32_t old_pointer_write;
	len_out = DATAFLASH_CreateData(data_save, data);
	if((SECTOR_SIZE - g_flashdata_poiter.cur_poiter_data%SECTOR_SIZE) >= len_out) // enough in sector
	{
		old_pointer_write = g_flashdata_poiter.cur_poiter_data;
		try_write_times = 5;
		while(try_write_times-- > 0  && ret == false)
		{
			ret = seial_flash.write(g_flashdata_poiter.cur_poiter_data, data_save, len_out);
			if(ret == true)
			{
				g_flashdata_poiter.cur_poiter_data += len_out;
				break;
			}
		}
	}
	else		// next sector
	{
		g_flashdata_poiter.cur_poiter_data = SECTOR_SIZE*((g_flashdata_poiter.cur_poiter_data/SECTOR_SIZE) + 1);
		is_new_sector = true;
		if(g_flashdata_poiter.cur_poiter_data >= ADDR_START_MEMORY + SECTOR_SIZE*NUM_SECTOR_DATA)
		{
			g_flashdata_poiter.cur_poiter_data = ADDR_START_MEMORY;
		}
		old_pointer_write = g_flashdata_poiter.cur_poiter_data;
		seial_flash.erase_sector(g_flashdata_poiter.cur_poiter_data);
		if(g_flashdata_poiter.cur_poiter_data + SECTOR_SIZE >= ADDR_START_MEMORY + SECTOR_SIZE*NUM_SECTOR_DATA)
		{
			seial_flash.erase_sector(ADDR_START_MEMORY);
			if(g_flashdata_poiter.miss_poiter_data >= ADDR_START_MEMORY && g_flashdata_poiter.miss_poiter_data < ADDR_START_MEMORY + SECTOR_SIZE)
			{
				g_flashdata_poiter.miss_poiter_data = ADDR_START_MEMORY + SECTOR_SIZE;
			}
		}
		else
			seial_flash.erase_sector(g_flashdata_poiter.cur_poiter_data + SECTOR_SIZE);
		try_write_times = 5;
		while(try_write_times-- > 0  || ret == false)
		{
			ret = seial_flash.write(g_flashdata_poiter.cur_poiter_data, data_save, len_out);
			if(ret == true)
			{
				g_flashdata_poiter.cur_poiter_data += len_out;
				break;
			}
		}
	}
	if(ret == true)
	{
		if((data->type & 0x80) == 0)
		{
			if(old_pointer_write == g_flashdata_poiter.miss_poiter_data) // no miss
			{
				g_flashdata_poiter.miss_poiter_data = g_flashdata_poiter.cur_poiter_data;
			}
		}
		if(is_new_sector == true && old_pointer_write < g_flashdata_poiter.miss_poiter_data) // if may over sector miss
		{
			if(g_flashdata_poiter.miss_poiter_data <  old_pointer_write + SECTOR_SIZE*2)
				g_flashdata_poiter.miss_poiter_data = old_pointer_write + SECTOR_SIZE*2;
			if(g_flashdata_poiter.miss_poiter_data >= ADDR_START_MEMORY + SECTOR_SIZE*NUM_SECTOR_DATA)
			{
				g_flashdata_poiter.miss_poiter_data = g_flashdata_poiter.miss_poiter_data - SECTOR_SIZE*NUM_SECTOR_DATA;
			}
		}
	}
	return ret;
}

DataContent* DATAFLASH_Read(uint32_t addr, uint16_t *offset)
{
	DataContent* ret = null;
	uint16_t pos;
	uint8_t data_read[MAX_DATA_LEN];
	seial_flash.read(ADDR_START_MEMORY + addr, data_read, MAX_DATA_LEN);
	ret = DATAFLASH_ExtractData(data_read, &pos);
	if(ret != null)
	{
		*offset = pos;
		return ret;
	}
	else
	{
		*offset = MAX_DATA_LEN/2;
	}
	return null;
}

DataContent* DATAFLASH_ReadMiss(uint16_t *offset)
{
	DataContent* ret = null;
	uint16_t pos;
	uint8_t data_read[MAX_DATA_LEN];
	check:
	if(g_flashdata_poiter.miss_poiter_data == g_flashdata_poiter.cur_poiter_data) // no miss
	{
		return null;
	}
	seial_flash.read(g_flashdata_poiter.miss_poiter_data, data_read, MAX_DATA_LEN);
	ret = DATAFLASH_ExtractData(data_read, &pos);
	if(ret != null)
	{
		g_flashdata_poiter.miss_poiter_data += pos;
		if((ret->type & 0x80) == 0x00) // is not miss data
		{
			DATA_Delete(&ret);
			goto check;
		}
		*offset = pos;
		return ret;
	}
	else
	{
		*offset = MAX_DATA_LEN/2;
	}
	return null;
}


