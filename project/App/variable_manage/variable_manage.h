/*
 * variable_manage.h
 *
 *  Created on: Aug 6, 2021
 *      Author: VDT-MS
 */

#ifndef APP_VARIABLE_MANAGE_VARIABLE_MANAGE_H_
#define APP_VARIABLE_MANAGE_VARIABLE_MANAGE_H_


#include "types.h"
#include "common.h"
#include "time.h"

/**
 * @brief module state 
 *
 * Detailed explanation.
 */
typedef volatile struct
{
	union 
	{
	    uint8_t Val;						/*!<   */
	    struct
	    {
	    	uint8_t gps:1;				/*!<   */
	    	uint8_t sos:1;				/*!<   */
	    	uint8_t door:1;				/*!<   */
	    	uint8_t acc:1;				/*!<   */
	        uint8_t air:1;				/*!<   */
	        uint8_t flash:1;				/*!<   */
	        uint8_t sd:1;					/*!<   */
	        uint8_t ov_speed:1;			/*!<   */
	    } State;
	}MotoState;
	
	struct 
	{
		float32_t Lat;
		float32_t Lng;
		uint16_t OrGPS;
	}Posistion;
	
	struct
	{
		uint8_t speed_gps;
		uint8_t speed_machine;
	}Speed;
}VehicleData;

/**
 * @brief module state 
 *
 * Detailed explanation.
 */
typedef volatile struct
{
	uint16_t	Curr_RunTime;			/*!<  Thoi gian lai lien tuc (phut) hien tai, khi dung 15p thi reset lai dung de bao cao*/
	uint16_t	CountStopTimes; 		/*!<  So lan dung xe */
	uint16_t	RunTimePDate;			/*!<  Tong thoi gian xe chay trong ngay */
	uint8_t 	CurrDate;
	uint16_t	DoorTimes;				/*!<  So lan mo cua */
	uint16_t	StopTimes;				/*!<  Thoi gian dung do*/
	uint16_t	OverSpeed;				/*!<  So lan chay qua toc do */
	float32_t	road;					/*!<  Quang duong */
	uint32_t	SumOfPulse; 			/*!<  Tong so xung */
	uint32_t	pulse_count;			/*!<  Quang duong xung tuc thoi*/
	uint8_t		LoginStatus;			/*!<  1:login 2:logout	3:loging*/
	uint8_t		driver_name[44];		/*!<  Ten lai xe*/
	uint8_t		driver_license[16]; 	/*!<  So giay phep lai xe*/
}DriverData;

/**
 * @brief module state 
 *
 * Detailed explanation.
 */
typedef volatile struct{
	uint16_t 		time_run_send;		/*!<   */
	uint16_t 		time_stop_send; 	/*!<   */
	uint16_t 		speedlimit; 		/*!<   */
	uint16_t 		time_dri_loop;		/*!<   */
	uint16_t 		time_dri_aday;		/*!<   */

	uint8_t			speed_method;		/*!<  */
	uint16_t		pulse_per_kilomet;
	uint8_t			system_cal_distance_method;
	float32_t		ratio_of_fuel;
	uint8_t 		ref_fuel_percent;
	uint32_t		ref_adc_value;
	
	uint8_t 		IMEI[16];			/*!<   */
	uint8_t 		IP[16];				/*!<   */
	uint8_t 		Port[6];			/*!<   */
	uint8_t 		ID_Track[11]; 	/*!<   */
	uint8_t			local_ip_addr[16];
	uint8_t			imsi[16];
	uint8_t			plate[11];
	uint8_t 		VIN[18];
	
	uint8_t			device_pass[9];
	uint8_t			admin_pass[9];
	uint8_t			user_pass[9];
}ConfigData; 

extern time_t 		g_system_time;
extern DriverData	g_driver_data;
extern ConfigData 	g_config_data;
extern VehicleData	g_vehicle_data;


#endif /* APP_VARIABLE_MANAGE_VARIABLE_MANAGE_H_ */
