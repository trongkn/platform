/*
 * test_save_data.c
 *
 *  Created on: Aug 17, 2021
 *      Author: VDT-MS
 */


#include <data_manage/data_create.h>
#include <data_manage/data_flash.h>
#include <timer/timer.h>
#include <variable_manage/variable_manage.h>
#include <time.h>
#include "stdio.h"
#include <windows.h>

static DataContent* data_test = null;
static DataContent* data_restore = null;
static uint32_t num_test = 0;
static uint32_t poiter_flash_read = 0;
static uint16_t len_out;

static timer_obj *timer1;

static void handle(void)
{
	g_system_time++;
}

int main(void)
{
	timer1  = create_timer(1000, &handle);
	timer1->start(timer1);

	data_restore = DATAFLASH_RestoreData();
	printf("current poiter data = %d, poiter miss = %d", g_flashdata_poiter.cur_poiter_data, g_flashdata_poiter.miss_poiter_data);
	while(1)
	{
		Sleep(10);
		data_test = DATA_CreateJourneyData();
		if(data_test != null)
		{
			data_test->type |= 0x80;
			if(DATAFLASH_Save(data_test) == true)
				num_test++;
			DATA_Delete(&data_test);
		}
		if(num_test >= 500)
			break;
	}
	printf("write miss = %d, sucesss = %d", 100, num_test);
	while(1)
	{
		Sleep(10);
		data_test = DATA_CreateJourneyData();
		if(data_test != null)
		{
			if(DATAFLASH_Save(data_test) == true)
				num_test++;
			DATA_Delete(&data_test);
		}
		if(num_test >= 500)
			break;
	}
	printf("write data = %d, sucesss = %d", 100, num_test);
	while(1)
	{
		data_test = DATAFLASH_ReadMiss(&len_out);
		if(data_test != null)
		{
			num_test++;
			DATA_Delete(&data_test);
		}
		else
		{
			break;
		}
	}
	printf("read miss = %d", num_test);
	data_test = DATA_create_backup_infor_data();
	if(data_test != null)
	{
		DATAFLASH_Save(data_test);
		DATA_Delete(&data_test);
	}
	printf("save poiter data = %d, poiter miss = %d", g_flashdata_poiter.cur_poiter_data, g_flashdata_poiter.miss_poiter_data);
	num_test = 0;
	while(1)
	{
		Sleep(10); // Sleep three seconds
		data_test = DATAFLASH_Read(poiter_flash_read, &len_out);
		poiter_flash_read += len_out;
		if(data_test != null)
		{
			if(data_test->type == DATA_JOURNEY)
				num_test++;
			DATA_Delete(&data_test);
		}
		if(num_test >= 100)
			break;
	}
	printf("write = %d, read = %d", 100, num_test);
}


